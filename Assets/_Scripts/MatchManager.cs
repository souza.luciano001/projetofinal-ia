using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class MatchManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerUI;
    [SerializeField] private TextMeshProUGUI botUI;

    private int playerScore = 0;
    private int botScore = 0;

    public int PlayerScore { get => playerScore; set => playerScore = value; }
    public int BotScore { get => botScore; set => botScore = value; }

    void Update()
    {
        playerUI.text = ""+PlayerScore;
        botUI.text = ""+BotScore;
    }
}
