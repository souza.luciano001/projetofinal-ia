using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCollider : MonoBehaviour
{
    public enum Type { player, bot }

    [SerializeField] private Type pointType;

    [SerializeField] private MatchManager manager;
    [SerializeField] private bool hasScored = false;

    public bool HasScored { get => hasScored; set => hasScored = value; }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Ball"))
        {
            switch (pointType)
            {
                case Type.player:
                    {
                        manager.BotScore += 1;
                    }
                    break;
                case Type.bot:
                    {
                        manager.PlayerScore += 1;
                        HasScored = true;
                    }
                    break;
            }
        }
    }


}
