using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class BotBehavior : Agent
{
    [Header("Bot Attributes")]
    [SerializeField] private float speed;
    [SerializeField] private bool isTraining = false;
    [SerializeField] private float xOffset = 0;
    [SerializeField] private float rewardValue = 1f;
    [SerializeField] private float punishValue = -1f;
    [SerializeField] private int ballWallCountToReset;

    [SerializeField] private float rewardHolder;
    [SerializeField] private Transform target;
    [SerializeField] private PointCollider pointer;


    private Rigidbody rb;
    private Vector3 movement;
    private float inputHolder;
    private bool collideOnWalls = false;
    private bool collideBall = false;

    public float RewardHolder { get => rewardHolder; set => rewardHolder = value; }
    public bool IsTraining { get => isTraining; set => isTraining = value; }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public override void OnEpisodeBegin()
    {
        if (IsTraining == true)
        {
            target.localPosition = new Vector3(xOffset, Random.Range(-4,4), transform.localPosition.z);
            target.GetComponent<BallBehavior>().Wallcount = 0;
        }

        collideOnWalls = false;
        collideBall = false;
        
        pointer.HasScored = false;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition.y);
        sensor.AddObservation(inputHolder);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        inputHolder = actions.ContinuousActions[0];

        movement = new Vector3(0, inputHolder * speed * Time.fixedDeltaTime, 0);

        rb.MovePosition(transform.position + movement);

        if (collideBall == true)
        {
            AddReward(rewardValue);
            rewardHolder += GetCumulativeReward();
            EndEpisode();
        }

        if (pointer.HasScored == true)
        {
            AddReward(punishValue);
            rewardHolder += GetCumulativeReward();
            EndEpisode();
        }

        if (collideOnWalls == true)
        {
            AddReward(punishValue);
            rewardHolder += GetCumulativeReward();
            EndEpisode();
        }

        if (target.GetComponent<BallBehavior>().Wallcount >= ballWallCountToReset && collideOnWalls == false && pointer.HasScored == false && collideBall == false)
        {
            EndEpisode();
        }


    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> continuousActions = actionsOut.ContinuousActions;
        continuousActions[0] = Input.GetAxis("Vertical");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            collideBall = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            collideOnWalls = true;
        }
    }

}
