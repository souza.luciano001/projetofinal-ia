using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlataformBehavior : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody rb;

    Vector3 movement;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        movement = new Vector3(0, Input.GetAxis("Vertical") * speed * Time.fixedDeltaTime, 0);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + movement);
    }
}
