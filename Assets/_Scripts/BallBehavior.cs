using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;

public class BallBehavior : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private BotBehavior agent;
    [SerializeField]private bool hasScored = false;

    private int wallcount = 0;
    private Rigidbody rb;
    public bool HasScored { get => hasScored; set => hasScored = value; }
    public int Wallcount { get => wallcount; set => wallcount = value; }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = StartBall();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Point"))
        {
            if (agent.IsTraining == false)
            {
                transform.localPosition = Vector3.zero;
            }
            rb.velocity = StartBall();
        }

        if (other.gameObject.CompareTag("Wall") && agent.IsTraining == true)
        {
            wallcount += 1;
        }
    }

    Vector3 StartBall() 
    {
        float speedX = Random.Range(0, 2) == 0 ? -1 : 1;
        float speedY = Random.Range(0, 2) == 0 ? -1 : 1;

        return new Vector3(speed * speedX, speed * speedY, 0f);
    }
}
