This is a student machine learning project using the ML-Agents SDK in Unity Engine, training a bot to play pong, using training concepts inspired by real ping pong training, any questions, send to souza.luciano001@gmail.com or on twitter @_lusithanos


This project have this scripts:

-BotBehavior.cs
This script is responsible for the bot's behavior, with parameters to modify the ia trainings

-BallBehavior.cs
This is the ball default behavior

-MatchManager.cs
This is the manager of the match, showing and holding the match score

-PlayerPlataformBehavior.cs
This is the script responsible to the player plataform input and movement in the game

-PointCollider.cs
this script is responsible for informing when one of the players (human or bot) scores when verifying the collision of the ball in a collider


And this Scenes:

-BotTrainingHalf.unity
This scene is made to the bot training using the half of gameplay space

-BotTraininigFull.unity
This scene is made to the bot training using the all of the gameplay space

-GameScene.unity
This scene is when player can play with a bot using a trained "brain"
